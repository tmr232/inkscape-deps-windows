# Windows build-time dependencies

This repository collects the msys2-based dependencies to build Inkscape on Windows. The heavy lifting is done by [`msys2installdeps.sh`](https://gitlab.com/inkscape/inkscape/-/blob/master/buildtools/msys2installdeps.sh) combined with minor additions for testing and packaging (see [`.gitlab-ci.yml`](.gitlab-ci.yml) for  details). The goal is to have a self-contained msys2 installation folder (`C:/msys64`) so we can archive it and use it in CI.

Releases are created manually by tagging a commit using the pattern `rYYMMDD.n`. Inkscape's CI can then be updated to use a newer release. While this creates an additional maintenance burden we gain the ability to build Inkscape against a fixed set of dependenies instead of a moving target (msys2 follows the rolling release model and does not provide an archive of old versions).

## license

[GPL-2.0-or-later](LICENSE)

### attributions

Repository logo taken from [Microsoft icons created by Pixel perfect - Flaticon](https://www.flaticon.com/free-icons/microsoft").
